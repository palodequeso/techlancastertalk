#include <iostream>
#include <vector>
#include <glm/glm.hpp>
#include <noise/noise.h>
#include <SFML/Graphics/Image.hpp>
#include <tbb/tbb.h>
#include "tbb/parallel_for_each.h"
#include "tbb/task_scheduler_init.h"
#include "terrain.h"

class noise_task {
public:
    noise_task(int start_x, int start_y, int resolution, Terrain *terrain, sf::Image *image) {
        m_start_x = start_x;
        m_start_y = start_y;
        m_resolution = resolution;
        m_terrain = terrain;
        m_image = image;
    }
    void operator()() {
        int current_x = 0;
        int current_y = 0;
        float value = 0.0f;
        int tex_color = 0;
        for (int px_index = 0; px_index < m_resolution; px_index++) {
            for (int py_index = 0; py_index < m_resolution; py_index++) {
                current_x = m_start_x + px_index;
                current_y = m_start_y + py_index;
                value = m_terrain->GetValue(current_x / double(m_resolution), current_y / double(m_resolution), 0.5);
                value = glm::clamp(value, -1.0f, 1.0f);
                tex_color = (value + 1.0f) / 2.0f * 255;
                m_image->setPixel(current_x, current_y, sf::Color(tex_color, tex_color, tex_color));
            }
        }
    }

private:
    sf::Image *m_image;
    Terrain *m_terrain;
    int m_start_x;
    int m_start_y;
    int m_resolution;
};

template <typename T> struct invoker {
  void operator()(T& it) const {it();}
};

int main(int argc, char **argv) {
    tbb::task_scheduler_init init;

    int dimension = 4;
    int resolution = 256;

    Terrain t;
    t.Generate(12345678);

    sf::Image image;
    int final_dim = dimension * resolution;
    image.create(final_dim, final_dim, sf::Color(0, 0, 0));

    std::vector<noise_task> tasks;

    int start_x = 0;
    int start_y = 0;
    for (int x_index = 0; x_index < dimension; x_index++) {
        start_x = x_index * resolution;
        for (int y_index = 0; y_index < dimension; y_index++) {
            start_y = y_index * resolution;
            tasks.push_back(noise_task(start_x, start_y, resolution, &t, &image));
        }
    }

    tbb::parallel_for_each(tasks.begin(), tasks.end(), invoker<noise_task>());

    image.saveToFile("terrain_parallel.png");

    return 0;
}
