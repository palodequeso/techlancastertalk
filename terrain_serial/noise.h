#ifndef MY_NOISE_H
#define MY_NOISE_H

#include <string>
#include <glm/glm.hpp>
#include <noise/noise.h>
#include <SFML/Graphics/Image.hpp>

class GradientPoint {
    public:
        GradientPoint(float _position, glm::vec4 _color);
        GradientPoint(float _position, glm::ivec4 _color);
        float position;
        glm::vec4 color;
};

class NoiseUtil {
    public:
        NoiseUtil(const noise::module::Module *_source_module);
        ~NoiseUtil(void);

        double GetValue(double x, double y, double z);
        double **GenerateCubeSphereHeights(glm::ivec2 dimensions);
        void CreateHeightMaps(double **heights, glm::ivec2 dimensions, std::string path);
        void CreateGradientMaps(double **heights, std::vector<GradientPoint> *gradient, glm::ivec2 source_dimensions,
                                glm::ivec2 target_dimensions, std::string path);
        void CreateNormalMaps(double **heights, glm::ivec2 dimensions, std::string path);
    private:
        const noise::module::Module *source_module;
        glm::vec4 GetGradientValue(std::vector<GradientPoint> *gradient, float index);
        std::string GetCubeMapSuffix(unsigned int side);
};

#endif
