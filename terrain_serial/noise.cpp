#include <iostream>
#include "noise.h"

NoiseUtil::NoiseUtil(const noise::module::Module *_source_module) {
    source_module = _source_module;
}

NoiseUtil::~NoiseUtil(void) {
    //
}

double NoiseUtil::GetValue(double x, double y, double z) {
    return source_module->GetValue(x, y, z);
}

double **NoiseUtil::GenerateCubeSphereHeights(glm::ivec2 dimensions) {
    double **heights = new double*[6];
    for (unsigned int i = 0; i < 6; i++) {
        heights[i] = new double[dimensions.x * dimensions.y];

        unsigned int height_index = 0;
        for (unsigned int y_index = 0; y_index < dimensions.y; y_index++){
            double y = ((y_index / double(dimensions.y)) * 2.0) - 1.0;
            for (unsigned int x_index = 0; x_index < dimensions.x; x_index++){
                double x = ((x_index / double(dimensions.x)) * 2.0) - 1.0;

                double out = 0.0;
                glm::vec4 in;
                if (i == 0){ // Front
                    in = glm::vec4(-x, y, -1.0, 1.0);
                }else if (i == 1){ // Back
                    in = glm::vec4(x, y, 1.0, 1.0);
                }else if (i == 4){ // Left
                    in = glm::vec4(-1.0, y, x, 1.0);
                }else if (i == 5){ // Right
                    in = glm::vec4(1.0, y, -x, 1.0);
                }else if (i == 2){ // Top
                    in = glm::vec4(x, -1.0, y, 1.0);
                }else if (i == 3){ // Bottom
                    in = glm::vec4(x, 1.0, -y, 1.0);
                }
                in = DS::Graphics::CubeToSphereMapping(in);

                out = source_module->GetValue(in.x, in.y, in.z);
                if (out < -1.0f) {
                    out = -1.0f;
                }
                if (out > 1.0f) {
                    out = 1.0f;
                }
                heights[i][height_index] = out;
                height_index += 1;
            }
        }
    }
    return heights;
}

void NoiseUtil::CreateHeightMaps(double **heights, glm::ivec2 dimensions, std::string path) {
    for (unsigned int i = 0; i < 6; i++) {
        double *h = heights[i];
        unsigned int h_index = 0;
        unsigned char *pixels = new unsigned char[dimensions.x * dimensions.y * 4];
        unsigned int p_index = 0;
        for (int y = 0; y < dimensions.y; y++) {
            for (int x = 0; x < dimensions.x; x++) {
                double height = h[h_index];

                unsigned char pixel_value = ((height + 1.0) / 2.0) * 255;
                pixels[p_index++] = pixel_value;
                pixels[p_index++] = pixel_value;
                pixels[p_index++] = pixel_value;
                pixels[p_index++] = 255;

                h_index += 1;
            }
        }

        sf::Image img;
        img.create(dimensions.x, dimensions.y, pixels);
        img.saveToFile(path + GetCubeMapSuffix(i));
    }
}

GradientPoint::GradientPoint(float _position, glm::vec4 _color) {
    position = _position;
    color = _color;
}

GradientPoint::GradientPoint(float _position, glm::ivec4 _color) {
    position = _position;
    color = glm::vec4(
        _color.x / 255.0f,
        _color.y / 255.0f,
        _color.z / 255.0f,
        _color.w / 255.0f
    );
}

glm::vec4 NoiseUtil::GetGradientValue(std::vector<GradientPoint> *gradient, float index) {
    std::vector<GradientPoint>::iterator iter = gradient->begin();
    while (iter != gradient->end()) {
        GradientPoint p = *iter;
        if (index < p.position) {
            break;
        }
        ++iter;
    }

    GradientPoint end = *iter;
    --iter;
    GradientPoint begin = *iter;

    float diff = end.position - begin.position;
    float di = index - begin.position;
    float end_amount = di / diff;
    float begin_amount = 1.0f - end_amount;

    return (begin.color * begin_amount) + (end.color * end_amount);
}

void NoiseUtil::CreateGradientMaps(double **heights, std::vector<GradientPoint> *gradient,
                                   glm::ivec2 source_dimensions, glm::ivec2 target_dimensions, std::string path) {
    float x_scale = source_dimensions.x / float(target_dimensions.x);
    float y_scale = source_dimensions.y / float(target_dimensions.y);
    for (unsigned int i = 0; i < 6; i++) {
        double *h = heights[i];
        unsigned int h_index = 0;
        unsigned char *pixels = new unsigned char[target_dimensions.x * target_dimensions.y * 4];
        unsigned int p_index = 0;
        for (int y = 0; y < target_dimensions.y; y++) {
            for (int x = 0; x < target_dimensions.x; x++) {
                h_index = (y * y_scale * source_dimensions.y) + (x * x_scale);
                double height = h[h_index];
                height = ((height + 1.0) / 2.0);
                glm::vec4 color = GetGradientValue(gradient, height);

                pixels[p_index++] = unsigned char(color.x * 255);
                pixels[p_index++] = unsigned char(color.y * 255);
                pixels[p_index++] = unsigned char(color.z * 255);
                pixels[p_index++] = unsigned char(color.w * 255);
            }
        }

        sf::Image img;
        img.create(target_dimensions.x, target_dimensions.y, pixels);
        img.saveToFile(path + GetCubeMapSuffix(i));
    }
}

void NoiseUtil::CreateNormalMaps(double **heights, glm::ivec2 dimensions, std::string path) {
    for (unsigned int i = 0; i < 6; i++) {
        double *h = heights[i];
        unsigned int h_index = 0;
        unsigned char *pixels = new unsigned char[dimensions.x * dimensions.y * 4];
        unsigned int p_index = 0;
        for (int y = 0; y < dimensions.y; y++) {
            for (int x = 0; x < dimensions.x; x++) {
                unsigned int up_index = h_index;
                if (y > 0) {
                    up_index -= dimensions.x;
                }
                unsigned int right_index = h_index;
                if (x < dimensions.x - 1) {
                    right_index = h_index + 1;
                }
                double height = h[h_index];
                double up = h[up_index];
                double right = h[right_index];

                // Calculate the surface normal.
                //nc *= bumpHeight;
                //nr *= bumpHeight;
                //nu *= bumpHeight;
                double ncr = (height - right);
                double ncu = (height - up);
                double d = sqrt ((ncu * ncu) + (ncr * ncr) + 1);
                double vxc = (height - right) / d;
                double vyc = (height - up) / d;
                double vzc = 1.0 / d;

                // Map the normal range from the (-1.0 .. +1.0) range to the (0 .. 255)
                // range.
                noise::uint8 xc, yc, zc;
                xc = (noise::uint8)((noise::uint)((floor)((vxc + 1.0) * 127.5)) & 0xff);
                yc = (noise::uint8)((noise::uint)((floor)((vyc + 1.0) * 127.5)) & 0xff);
                zc = (noise::uint8)((noise::uint)((floor)((vzc + 1.0) * 127.5)) & 0xff);

                //return Color (xc, yc, zc, 0);

                //unsigned char pixel_value = ((height + 1.0) / 2.0) * 255;
                pixels[p_index++] = xc;
                pixels[p_index++] = yc;
                pixels[p_index++] = zc;
                pixels[p_index++] = 255;

                h_index += 1;
            }
        }

        sf::Image img;
        img.create(dimensions.x, dimensions.y, pixels);
        img.saveToFile(path + GetCubeMapSuffix(i));
    }
}

std::string NoiseUtil::GetCubeMapSuffix(unsigned int side) {
    switch (side) {
        case CUBE_MAP_PX:
            return "_px.png";
        case CUBE_MAP_NX:
            return "_nx.png";
        case CUBE_MAP_PY:
            return "_py.png";
        case CUBE_MAP_NY:
            return "_ny.png";
        case CUBE_MAP_PZ:
            return "_pz.png";
        case CUBE_MAP_NZ:
            return "_nz.png";
    }
    return "";
}
