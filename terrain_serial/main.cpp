#include <iostream>
#include <glm/glm.hpp>
#include <noise/noise.h>
#include <SFML/Graphics/Image.hpp>
#include "terrain.h"

void could_be_done_on_a_thread(int start_x, int start_y, int resolution,
                               Terrain *t, sf::Image *image) {
    int current_x = 0;
    int current_y = 0;
    float value = 0.0f;
    int tex_color = 0;
    for (int px_index = 0; px_index < resolution; px_index++) {
        for (int py_index = 0; py_index < resolution; py_index++) {
            current_x = start_x + px_index;
            current_y = start_y + py_index;
            value = t->GetValue(current_x / double(resolution), current_y / double(resolution), 0.5);
            value = glm::clamp(value, -1.0f, 1.0f);
            tex_color = (value + 1.0f) / 2.0f * 255;
            image->setPixel(current_x, current_y, sf::Color(tex_color, tex_color, tex_color));
        }
    }
}

int main(int argc, char **argv) {
    int dimension = 4;
    int resolution = 256;

    Terrain t;
    t.Generate(12345678);

    sf::Image image;
    int final_dim = dimension * resolution;
    image.create(final_dim, final_dim, sf::Color(0, 0, 0));

    int start_x = 0;
    int start_y = 0;
    for (int x_index = 0; x_index < dimension; x_index++) {
        start_x = x_index * resolution;
        for (int y_index = 0; y_index < dimension; y_index++) {
            start_y = y_index * resolution;
            could_be_done_on_a_thread(start_x, start_y, resolution, &t, &image);
        }
    }
    image.saveToFile("terrain_serial.png");

    return 0;
}
