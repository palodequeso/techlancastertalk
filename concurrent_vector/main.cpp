#include <iostream>
#include <vector>
#include <tbb/tbb.h>

// The vector does not grow in a thread safe manner!
// Memory can get moved around or may have a double allocation
std::vector<int> myvec;
//tbb::concurrent_vector<int> myvec;

void add_element(int i) {
    myvec.push_back(i);
}

int main(int argc, char **argv) {
    const int size = 100000;

    tbb::parallel_for(0, size, 1, [=](int i) {
        add_element(i);
    });
    std::cout << "Vec Size: " << myvec.size() << std::endl;

    return 0;
}
