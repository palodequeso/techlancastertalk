## Talking Points:

- Growing number of cores.
- Process Overhead vs Thread Safety
    - Processes take more overhead to start up
    - Interprocess communication has more latency
    - Interprocess communication has more overhead (IPC, etc)
- Evolution of real-time applications
    - Games
        - Doom - Single Threaded
        - Half-life - Threaded game saves and some networking
        - Unreal Engine 3 - Threaded Resource Loading and Networking, Physics
        - Unreal Engine 4 - Task Based Modern Architecture
- Libraries
    - Intel TBB
    - Apple GCD
    - What else?
- Typical Game Loop
    ![Typical Game Loop](typical_game_loop.png)
- Modern Game Loop
    ![Modern Game Loop](modern_game_loop.png)
- Now we've stated the obvious
    - We need to utilize these cores more effectively for real-time,
      high requirement applications.
- Naive Examples
    - Noise and Terrain Demos
- Concurrent Container Example
    - Show std::vector
    - Show tbb::concurrent_vector
    - Explain that a naive approach to just using std::thread and std::vector
      might be to just use locks, but there's better ways. Intel TBB also
      allocates memory in a better way so it doesn't always have to lock.
- Languages
    - GO is awesome but sometimes you need to squeeze every little bit of
      performance http://blog.grok.se/2013/10/on-comparing-languages-c-and-go/
    - RUST has a lot of promise to be as fast and powerful as c or c++ with a
      lot more safety, but it's in flux and if you need a proven set of tools
      then this isn't really an option just yet.

### Links
https://developer.apple.com/Library/ios/documentation/Performance/Reference/GCD_libdispatch_Ref/Reference/reference.html
https://www.threadingbuildingblocks.org/
